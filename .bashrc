# ~/.bashrc

# if not running interactively, don't do anything
[ -z "$PS1" ] && return

# prompt
PS1="\[\033[01m\]\u@\h:\[\e[01;34m\]\w\[\e[37m\]\$\[\e[00m\] "

# title
PS1="\[\e]0;\u@\h:\w\a\]$PS1"

# alias
alias q="exit"
alias diff="diff --color=auto"
alias grep="grep --color=auto"
alias ls="ls --color=auto"
alias ll="ls -AF1"
alias lll="ls -AFl"

# history
HISTCONTROL="ignoreboth:erasedups"
HISTIGNORE="exit:q"
PROMPT_COMMAND=${PROMPT_COMMAND:+${PROMPT_COMMAND%;}; }"history -a; history -c; history -r;"

# bash completion
[ -f /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# osc7
osc7_cwd() {
    local strlen=${#PWD}
    local encoded=""
    local pos c o
    for (( pos=0; pos<strlen; pos++ )); do
        c=${PWD:$pos:1}
        case "$c" in
            [-/:_.!\'\(\)~[:alnum:]] ) o="${c}" ;;
            * ) printf -v o '%%%02X' "'${c}" ;;
        esac
        encoded+="${o}"
    done
    printf '\e]7;file://%s%s\e\\' "${HOSTNAME}" "${encoded}"
}
PROMPT_COMMAND=${PROMPT_COMMAND:+${PROMPT_COMMAND%;}; }osc7_cwd
