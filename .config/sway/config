# vars #########################################################################
################################################################################

set $mod   mod4
set $left  h
set $down  j
set $up    k
set $right l

set $black         #000000
set $white         #e2e2e2
set $blue          #856cff
set $magenta       #ff1170
set $brightmagenta #ffb1c0
set $red           #fa3500
set $orange        #d06600
set $green         #009843


# configuration ################################################################
################################################################################

workspace_layout tabbed
xwayland disable

# class                 border         background     text   indicator
client.focused          $magenta       $magenta       $black $red
client.focused_inactive $brightmagenta $brightmagenta $black $black
client.unfocused        $blue          $blue          $black $black
client.urgent           $red           $red           $black $black

default_border normal 0
default_floating_border normal 0
focus_wrapping yes
font monospace 13

input "type:keyboard" {
	xkb_options  caps:escape,compose:ralt
	repeat_delay 200
	repeat_rate  30
}
input "type:touchpad" tap enabled

seat * hide_cursor when-typing enable
output * background $black solid_color


# windows ######################################################################
################################################################################

# floating
bindsym $mod+shift+f floating toggle

# focus
bindsym $mod+$up    focus up
bindsym $mod+$down  focus down
bindsym $mod+$left  focus left
bindsym $mod+$right focus right
bindsym $mod+a      focus parent
bindsym $mod+s      focus child
bindsym $mod+g      focus mode_toggle

# fullscreen
bindsym $mod+f fullscreen toggle

# move
bindsym $mod+shift+$up          move up
bindsym $mod+shift+$down        move down
bindsym $mod+shift+$left        move left
bindsym $mod+shift+$right       move right
bindsym $mod+colon              move scratchpad

# scratchpad
bindsym $mod+semicolon scratchpad show

# sticky
bindsym $mod+shift+g sticky toggle

# resize
bindsym $mod+control+$left  resize shrink width
bindsym $mod+control+$down  resize grow height
bindsym $mod+control+$up    resize shrink height
bindsym $mod+control+$right resize grow width


# workspaces ###################################################################
################################################################################

set $wsq 0:q:bot
set $wsw 1:w:www
set $wse 2:e:txt
set $wsr 3:r:doc
set $wst 4:t:cmd
set $wsy 5:y:cmd
set $wsu 6:u:cmd
set $wsi 7:i:cmd
set $wso 8:o:cmd
set $wsp 9:p:top

bindsym $mod+tab          workspace back_and_forth
bindsym $mod+q            workspace $wsq
bindsym $mod+w            workspace $wsw
bindsym $mod+e            workspace $wse
bindsym $mod+r            workspace $wsr
bindsym $mod+t            workspace $wst
bindsym $mod+y            workspace $wsy
bindsym $mod+u            workspace $wsu
bindsym $mod+i            workspace $wsi
bindsym $mod+o            workspace $wso
bindsym $mod+p            workspace $wsp
bindsym $mod+bracketleft  workspace prev
bindsym $mod+bracketright workspace next

bindsym $mod+shift+tab          move workspace back_and_forth, workspace back_and_forth
bindsym $mod+shift+q            move workspace $wsq, workspace $wsq
bindsym $mod+shift+w            move workspace $wsw, workspace $wsw
bindsym $mod+shift+e            move workspace $wse, workspace $wse
bindsym $mod+shift+r            move workspace $wsr, workspace $wsr
bindsym $mod+shift+t            move workspace $wst, workspace $wst
bindsym $mod+shift+y            move workspace $wsy, workspace $wsy
bindsym $mod+shift+u            move workspace $wsu, workspace $wsu
bindsym $mod+shift+i            move workspace $wsi, workspace $wsi
bindsym $mod+shift+o            move workspace $wso, workspace $wso
bindsym $mod+shift+p            move workspace $wsp, workspace $wsp
bindsym $mod+shift+bracketleft  move workspace prev, workspace prev
bindsym $mod+shift+bracketright move workspace next, workspace next


# modes ########################################################################
################################################################################

set $session_mode "session: (r)eload (l)ock (q)uit (s)uspend (h)ibernate re(b)oot (p)oweroff"
mode $session_mode {
       bindsym {
               r      reload, mode default
               l      exec $swaylock, mode default
               q      exit, mode default
               s      exec systemctl suspend, mode default
               h      exec systemctl hibernate, mode default
               b      exec systemctl reboot, mode default
               p      exec systemctl poweroff, mode default
               escape mode default
       }
}
bindsym $mod+escape mode $session_mode, fullscreen disable

set $layout_mode  "layout: split(h) split(v) (s)tacking (t)abbed"
mode $layout_mode {
       bindsym {
               h      layout splith, mode default
               v      layout splitv, mode default
               s      layout stacking, mode default
               t      layout tabbed, mode default
               escape mode default
       }
}
bindsym $mod+d mode $layout_mode, fullscreen disable


# laptop #######################################################################
################################################################################

set $notify_hint string:x-canonical-private-synchronous

bindsym XF86AudioRaiseVolume exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%+, exec notify-send -e -t 2000 -h $notify_hint:audio "$(wpctl get-volume @DEFAULT_AUDIO_SINK@)"
bindsym XF86AudioLowerVolume exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%-, exec notify-send -e -t 2000 -h $notify_hint:audio "$(wpctl get-volume @DEFAULT_AUDIO_SINK@)"
bindsym XF86AudioMute        exec wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle, exec notify-send -e -t 2000 -h $notify_hint:audio "$(wpctl get-volume @DEFAULT_AUDIO_SINK@)"
bindsym XF86AudioMicMute     exec wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle, exec notify-send -e -t 2000 -h $notify_hint:audio "$(wpctl get-volume @DEFAULT_AUDIO_SOURCE@)"

bindsym XF86MonBrightnessUp         exec brightnessctl -qn set +1%, exec notify-send -e -t 2000 -h $notify_hint:brightness "Brightness: $(brightnessctl get)"
bindsym XF86MonBrightnessDown       exec brightnessctl -qn set 1%-, exec notify-send -e -t 2000 -h $notify_hint:brightness "Brightness: $(brightnessctl get)"
bindsym shift+XF86MonBrightnessUp   exec brightnessctl -qn set +10%, exec notify-send -e -t 2000 -h $notify_hint:brightness "Brightness: $(brightnessctl get)"
bindsym shift+XF86MonBrightnessDown exec brightnessctl -qn set 10%-, exec notify-send -e -t 2000 -h $notify_hint:brightness "Brightness: $(brightnessctl get)"


# run ##########################################################################
################################################################################

bindsym $mod+return  exec $TERMINAL
bindsym $mod+n       exec makoctl dismiss
bindsym $mod+shift+n exec makoctl restore
bindsym print        exec grim - | wl-copy
bindsym $mod+space   exec fuzzel
bindsym $mod+shift+space exec cd_open.sh


# autorun ######################################################################
################################################################################

exec systemctl --user import-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP
exec dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP=sway

exec mako --max-visible=0
exec devmon --exec-on-drive 'notify-send "Mounted %f at %d"' --exec-on-unmount 'notify-send "Unmounted %f"'
exec foot -s
exec wlsunset -l 45 -L 15

set $swaylock swaylock -f \
	--color=$black \
	--indicator-radius 1200 \
	--inside-color=$blue \
	--inside-clear-color=$green \
	--inside-caps-lock-color=$orange \
	--inside-ver-color=$magenta \
	--inside-wrong-color=$red \
	--text-color=#00000000 \
	--text-clear-color=#00000000 \
	--text-ver-color=#00000000 \
	--text-wrong-color=#00000000

exec swayidle -w \
	timeout  300 'brightnessctl -s set 1%' resume 'brightnessctl -r' \
	timeout  600 '$swaylock' \
	timeout  900 'swaymsg -q "output * power off"' resume 'swaymsg -q "output * power on"' \
	timeout 1800 'systemctl suspend-then-hibernate' \
	before-sleep '$swaylock'


# bar ##########################################################################
################################################################################

bar {
	separator_symbol "  "
	status_command exec swaystatus
	status_edge_padding 0
	status_padding 0
	strip_workspace_numbers yes
	wrap_scroll yes
	colors {
		background $blue
		statusline $black
		separator  $black
		#                  border         background     text
		focused_workspace  $magenta       $magenta       $black
		active_workspace   $brightmagenta $brightmagenta $black
		inactive_workspace $blue          $blue          $black
		urgent_workspace   $red           $red           $black
	}
}
