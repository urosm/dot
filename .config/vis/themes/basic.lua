-- define additional cursor styles on WIN_OPEN
vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	win['STYLE_CURSOR_'..vis.modes.OPERATOR_PENDING] = win.STYLE_LEXER_MAX - 1
	win['STYLE_CURSOR_'..vis.modes.VISUAL] = win.STYLE_LEXER_MAX - 2
	win['STYLE_CURSOR_'..vis.modes.VISUAL_LINE] = win.STYLE_LEXER_MAX - 3
	win['STYLE_CURSOR_'..vis.modes.INSERT] = win.STYLE_LEXER_MAX - 4
	win['STYLE_CURSOR_'..vis.modes.REPLACE] = win.STYLE_LEXER_MAX - 5
	win:style_define(
		win['STYLE_CURSOR_'..vis.modes.OPERATOR_PENDING],
		'fore:black,back:red')
	win:style_define(
		win['STYLE_CURSOR_'..vis.modes.VISUAL],
		'fore:black,back:yellow')
	win:style_define(
		win['STYLE_CURSOR_'..vis.modes.VISUAL_LINE],
		'fore:black,back:yellow')
	win:style_define(
		win['STYLE_CURSOR_'..vis.modes.INSERT],
		'fore:black,back:magenta')
	win:style_define(
		win['STYLE_CURSOR_'..vis.modes.REPLACE],
		'fore:black,back:green')
end)

-- apply additional cursor styles on WIN_STATUS
vis.events.subscribe(vis.events.WIN_STATUS, function(win)
	local pos = win.selection.pos
	if not pos then return end
	local style_id = win ~= vis.win and
		win['STYLE_CURSOR'] or
		win['STYLE_CURSOR_'..vis.mode] or
		win['STYLE_CURSOR_PRIMARY']
	win:style(style_id, pos, pos)
end)

local lexers = vis.lexers
lexers.STYLE_LINENUMBER = 'fore:blue'
lexers.STYLE_LINENUMBER_CURSOR = 'fore:magenta'
lexers.STYLE_CURSOR = 'fore:black,back:white'
lexers.STYLE_CURSOR_PRIMARY = 'fore:black,back:blue'
lexers.STYLE_CURSOR_LINE = 'underlined'
lexers.STYLE_COLOR_COLUMN = 'fore:black,back:blue'
lexers.STYLE_SELECTION = 'fore:black,back:white'
lexers.STYLE_STATUS = 'fore:blue'
lexers.STYLE_STATUS_FOCUSED = 'fore:blue'
lexers.STYLE_SEPARATOR = 'fore:blue'
lexers.STYLE_INFO = 'fore:red'
lexers.STYLE_EOF = 'fore:blue'

lexers.STYLE_NOTHING = ''
lexers.STYLE_WHITESPACE = ''

-- programming languages
lexers.STYLE_DEFAULT = ''
lexers.STYLE_COMMENT = 'fore:blue,bold'
lexers.STYLE_STRING = 'fore:red,bold'
lexers.STYLE_NUMBER = 'fore:red,bold'
lexers.STYLE_KEYWORD = 'fore:yellow,bold'
lexers.STYLE_IDENTIFIER = ''
lexers.STYLE_OPERATOR = 'fore:cyan,bold'
lexers.STYLE_ERROR = 'fore:red,italics'
lexers.STYLE_PREPROCESSOR = 'fore:magenta,bold'
lexers.STYLE_CONSTANT = 'fore:cyan,bold'
lexers.STYLE_CONSTANT_BUILTIN = 'fore:cyan,bold'
lexers.STYLE_VARIABLE = 'fore:white,bold'
lexers.STYLE_VARIABLE_BUILTIN = 'fore:blue,bold'
lexers.STYLE_FUNCTION = 'fore:white,bold'
lexers.STYLE_FUNCTION_BUILTIN = 'fore:white,bold'
lexers.STYLE_FUNCTION_METHOD = 'fore:white,bold'
lexers.STYLE_CLASS = 'fore:yellow,bold'
lexers.STYLE_TYPE = 'fore:green,bold'
lexers.STYLE_LABEL = 'fore:green,bold'
lexers.STYLE_REGEX = 'fore:green,bold'
lexers.STYLE_EMBEDDED = 'back:blue,bold'
lexers.STYLE_ANNOTATION = ''

-- markup languages
lexers.STYLE_TAG = lexers.STYLE_KEYWORD
lexers.STYLE_ATTRIBUTE = 'fore:green,bold'
lexers.STYLE_HEADING = 'fore:white,bold'
lexers.STYLE_BOLD = 'bold'
lexers.STYLE_ITALIC = 'italics'
lexers.STYLE_UNDERLINE = 'underlined'
lexers.STYLE_CODE = 'fore:yellow'
lexers.STYLE_LINK = lexers.STYLE_KEYWORD
lexers.STYLE_REFERENCE = lexers.STYLE_KEYWORD
lexers.STYLE_LIST = lexers.STYLE_KEYWORD

-- Markdown
lexers.STYLE_DELIMITER_MARKDOWN = 'fore:blue'
lexers.STYLE_HEADING_MARKDOWN = 'bold'
lexers.STYLE_HR_MARKDOWN = lexers.STYLE_DELIMITER_MARKDOWN
lexers.STYLE_CODE_MARKDOWN = 'fore:yellow'
lexers.STYLE_REFERENCE_MARKDOWN = lexers.STYLE_REFERENCE
lexers.STYLE_LINK_MARKDOWN = lexers.STYLE_REFERENCE
