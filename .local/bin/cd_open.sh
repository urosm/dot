#!/bin/sh

sel="$(find -type d | fuzzel -dp'cd > ')"

[ -n "$sel" ] || exit

[ -d "$sel" ] && cd "$sel" || mkdir -p "$sel" && cd "$sel"

sel="$(find -type f | fuzzel -dp'open > ')"

case $(mimetype -b "$sel") in 
	text/*|\
	application/x-shellscript) exec $TERMINAL "$EDITOR" "$sel";;
	*) exec xdg-open "$sel" || exec $TERMINAL "$EDITOR" "$sel";;
esac
