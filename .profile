# ~/.profile

# include .bashrc if running bash
[ -n "$BASH_VERSION" ] && [ -f "$HOME"/.bashrc ] && . "$HOME"/.bashrc

# set PATH so it includes user's private bin if it exists
[ -d "$HOME"/.local/bin ] && PATH="$HOME/.local/bin:$PATH"

# xdg base directory specification
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_STATE_HOME="$HOME"/.local/state
export XDG_DATA_DIRS=/usr/local/share:/usr/share
export XDG_CONFIG_DIRS=/etc/xdg

# .bash_history
export HISTFILE="$XDG_STATE_HOME"/bash/history
[ -d "$XDG_STATE_HOME"/bash ] || mkdir -p "$XDG_STATE_HOME"/bash

# .inputrc
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc

# .npm/, .npmrc
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc

# .aspell.conf etc.
[ -d "$XDG_DATA_HOME"/aspell ] || mkdir -p "$XDG_DATA_HOME"/aspell
export ASPELL_CONF="per-conf $XDG_CONFIG_HOME/aspell/aspell.conf; personal $XDG_DATA_HOME/aspell/sl.pws; repl $XDG_DATA_HOME/aspell/sl.prepl"

# editor
export EDITOR=vi

# desktop
if [ "$(tty)" = "/dev/tty1" ] && command -v sway >/dev/null; then
	export LANG=sl_SI.UTF-8
	export LC_MESSAGES=en_US.UTF-8
	export TERMINAL=footclient
	# run sway
	export WLR_RENDERER=vulkan
	exec systemd-cat -t sway sway
fi
